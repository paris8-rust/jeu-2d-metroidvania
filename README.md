Voici un lien si vous n'etes pas trop familiarisé avec le terme "Metroidvania":
https://fr.wikipedia.org/wiki/Metroidvania


Objectifs:
- Créer une mape pour le jeu, avec plusieurs écrans/niveaux, des zones ou on ne pourra pas y acceder normalement.
- Faire un personnage controlé par l'utilisateur.
- Faire des ennemis, plusieurs types, si possible (ennemi basique, a distance, tank, volant...).
- Placer des upgrades dans la mape qui permettront au personnage d'acceder a de nouvelles zones.
- Peut etre un boss final, on verra.
- Si j'ai le temps, je voudrait aussi faire des modèles sur gimp pour le personnage et les ennemis et les animer, ultérieurement ce serait bien d'ajouter du design visuel pour la mape, mais ce serait plus un travail sur gimp ou autres que sur Rust, du coup ça va se faire uniquement si j'ai le temps (et la foi), si non le jeu sera fait a base de rectangles colorés à la Metroid 1986.


N.B.: Il se peut que ce projet ne soit pas du tout réaliste, faites-le moi remarquer si c'est le cas.

